﻿using System;
using Xunit;
using StoneAge.Game;

namespace Tests
{
    public class PlayerTests
    {
        private readonly Player player;

        public PlayerTests()
        {
            player = new Player();
        }

        [Theory]
        [InlineData(1, 6)]
        public void TestAddDevelopers(int num, int expected)
        {
            player.NumDevelopers += num;
            int result = player.NumDevelopers;
            Assert.Equal(expected, result);
        }
    }
}
